/*YULDUZ KHODJAEVA*/
/*Program for detecting the red points and printing the binary image in the second frame*/
/*with displaying coordinates of the point*/
/*IMPORTANT: the program prints the coordinates of those points whose area is greater than 3000*/
/*opencv version 2.4.13 is used */


#include <opencv2\highgui\highgui.hpp>
#include <opencv2\objdetect\objdetect.hpp>
#include <opencv2\imgproc\imgproc.hpp>       
#include <opencv\cv.h>
#include <opencv\highgui.h>
#include <iostream>
#include <string>

using namespace std;
using namespace cv;


CvSeq* contours = 0; // for storing contours of the image
CvMemStorage* storage = NULL; 
IplImage *currentFrame, *RedObjectFrame, *tempRedObjFrame; // currentFrame for displaying current window, RedObjectFrame for displaying second window with detected object
CvCapture* cam;

int main(){

	CvFont font;
	cvInitFont(&font, CV_FONT_HERSHEY_SIMPLEX, 0.5, 0.5); // initializing  font to use in cvPutText() function

	Mat mask1;                             // result for red object range will be stored here
	cam = cvCaptureFromCAM(0);             // start video-capture

	storage = cvCreateMemStorage(0);

	currentFrame = cvQueryFrame(cam);      // cvQueryFrame(object of CvCapture)

	CvSize imgSize;

	imgSize.height = currentFrame->height; // setting the height and
	imgSize.width = currentFrame->width;   // width based on the currentFrame (image captured by WEB-Camera)

	RedObjectFrame = cvCreateImage(imgSize, IPL_DEPTH_8U, 1); // initializing our second frame


	while (1){
		currentFrame = cvQueryFrame(cam);
		if (!currentFrame)                 // if currentFrame is NULL
			break;

		Mat current_frame(currentFrame);      // from IplImage object ->Mat object
		Mat red_object(RedObjectFrame); 

		cvtColor(current_frame, red_object, COLOR_BGR2HSV); // convert from BGR to HSV to fit range [0,1]

		inRange(red_object, Scalar(173, 70, 50), Scalar(179, 255, 255), mask1); // checks within the range of red RGB (plain red is also detected)
		
		RedObjectFrame = cvCloneImage(&(IplImage)mask1); // convert from MAT object -> IplImage object

		cvSmooth(RedObjectFrame, RedObjectFrame, CV_BLUR);

		cvThreshold(RedObjectFrame, RedObjectFrame, 25, 255, CV_THRESH_BINARY);

		tempRedObjFrame = cvCloneImage(RedObjectFrame); // temporary copying RedObjectFrame -> tempRedObjFrame

		cvFindContours(tempRedObjFrame, storage, &contours); // finding contours of tempRedObjFrame
		
		
		for (; contours != 0; contours = contours->h_next){
			CvPoint2D32f center; // for storing x & y coordinates of point
			float radius = 0;
			
			cvMinEnclosingCircle(contours, &center, &radius); // finds the contours and its minimum circle 
			string X = to_string((int)center.x);
			string Y = to_string((int)center.y);
			string text = X +","+ Y; // string concatenation 
			float area_of_circle = 3.14*radius*radius; // calculating the area of cirlce in order to print only those points whose area exceed 3000
			
			if (area_of_circle > 3000){
			cvPutText(RedObjectFrame, text.c_str(), cvPoint((int)center.x, (int)center.y), &font, cvScalar(255, 0, 0)); // put <X,Y> coordinates into second frame
			}
		}
		// do integer to string conversion here
		cvShowImage("Output Image", currentFrame);

		//display threshold image
		cvShowImage("Difference image", RedObjectFrame);

		cvClearMemStorage(storage);
		contours = 0;
		char c = cvWaitKey(33);
		if (c == 27) break;
	}

	cvReleaseImage(&RedObjectFrame);
	cvReleaseImage(&currentFrame);
	return 0;
}